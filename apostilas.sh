mkdir $1
echo ======================================
cd $1 && echo '        Iniciando '$1'...'
echo ======================================
wget --header "User-Agent: Mozilla/5.0 (Windows NT 5.2; rv:2.0.1) Gecko/20100101 Firefox/4.0.1" --header "Upgrade-Insecure-Requests: 1" --header "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" --header "Cookie: sophia_u=\"QlPUwPNGQyXceGiuZ3GfGeoiG7Wupaabr0giYNPV7XprK2j9S4rhWXGRxKZbt5EswDcrBV8xzijXHA0HPEI6hg==\"; JSESSIONID=A215DE152BB2A9D72A8F0179C1FCAE2C" http://digital.cneceduca.com.br/material-didatico/$2/2a-serie-do-ensino-medio/a/a/files/assets/common/page-html5-substrates/page0{001..200}_l.jpg


for file in *.jpg; do
	tesseract $file -l por ${file}.pdf pdf
done
cd ..

echo 'COMPRIMINDO...'

gs -sDEVICE=pdfwrite \
	-dCompatibilityLevel=1.4 \
	-dPDFSETTINGS=/ebook \
	-dNOPAUSE \
	-dFIXEDMEDIA \
	-dPDFFitPage \
	-dBATCH \
	-sPAPERSIZE=a4 \
	-sOutputFile=apostila2/$1.pdf $1/*.pdf

