\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{frederico}[13/03/2017 Classe para uso em arquivos de caderno]
\LoadClass[a4paper, 12pt]{article}

\RequirePackage[utf8]{inputenc}
\RequirePackage[brazil]{babel}
\RequirePackage{makeidx}
\RequirePackage{indentfirst}
\RequirePackage{icomma}
\RequirePackage{multirow}
\RequirePackage{geometry}
\RequirePackage{textcomp}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{tikz}
\RequirePackage{pgfplots}
\RequirePackage{array}
\RequirePackage{enumitem}
\RequirePackage{multicol}
\RequirePackage{chemfig}
\RequirePackage{mhchem}
\RequirePackage{amssymb}

\geometry{a4paper, left=30mm, top=30mm, right=20mm, bottom=20mm}
\setlist[itemize]{noitemsep}
\everymath{\displaystyle}

\setangleincrement{30}

\newcommand{\titulo}{
	\pagenumbering{gobble}
	\begin{titlepage}
	\begin{center}
		{\textbf{COLÉGIO CENECISTA FREDERICO MICHAELSEN}}\\\vspace{2cm}
		\@author\\\vspace{8cm}
		{\LARGE \@title}\\\vspace{5mm}
		{\Large ~~~~~~}\\
		\vfill
		Nova Petrópolis - RS\\
		2017
	\end{center}
	\end{titlepage}
	\pagenumbering{roman}
	\tableofcontents
	\clearpage
	\pagenumbering{arabic}
}

%HELVETICA
%\usepackage{helvet}
%\renewcommand{\familydefault}{\sfdefault}

%NOVA PAGINA SEÇOES
\let\stdsection\section
\renewcommand\section{\newpage\stdsection}

\newcommand{\e}[1]{\times 10^{#1}}
\newcommand{\destacar}[1]{{\color{red}#1}}

\newcommand{\cinza}[1]{{\color{gray}#1}}
