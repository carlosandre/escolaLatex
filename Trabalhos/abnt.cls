\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{abnt}[13/03/2017 Normas da ABNT para LaTeX]
\LoadClass[a4paper, 12pt]{article}

\RequirePackage[utf8]{inputenc}
\RequirePackage{indentfirst}
\RequirePackage{geometry}
\RequirePackage{enumitem}
\RequirePackage{amsmath}
\RequirePackage{graphicx}
\RequirePackage[brazil]{babel}
\RequirePackage{times}
\RequirePackage{textcomp}
\RequirePackage{titlesec}
\RequirePackage{helvet}

\geometry{a4paper, left=30mm, top=30mm, right=20mm, bottom=20mm}

\titleformat*{\section}{\Large\bfseries\sffamily}

\def\@seccntformat#1{%
  \expandafter\ifx\csname c@#1\endcsname\c@section\else
  \csname the#1\endcsname\quad
  \fi} %REMOVER NUMEROS DE SEÇÃO

%\renewcommand\thesection{}

\setlist[itemize]{noitemsep}
\everymath{\displaystyle}

\newcommand{\titulo}{
	\pagenumbering{gobble}
	\begin{titlepage}
	\begin{center}
		{\textbf{COLÉGIO CENECISTA FREDERICO MICHAELSEN}}\\\vspace{36pt}
		\@author\\\vspace{190pt}
		{\Large \@title}\\
		\vfill
		Nova Petrópolis - RS\\
		2017
	\end{center}
	\end{titlepage}
    
	\pagenumbering{roman}
    
    \newpage
	\tableofcontents
    \newpage
    
	\pagenumbering{arabic}
}

\renewenvironment{quote}{

\vspace{15mm plus 2mm minus 2mm}

\setlength{\leftskip}{4cm}

\noindent \small}{\vspace{15mm plus 2mm minus 2mm}
    
\setlength{\leftskip}{0cm}}

\let\stdsection\section
\renewcommand\section{\newpage\stdsection}
