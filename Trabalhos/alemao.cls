\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{alamao}[13/03/2017 Classe para uso em arquivos de caderno]
\LoadClass[a4paper, 15pt]{article}

\RequirePackage[utf8]{inputenc}
\RequirePackage[brazil]{babel}
\RequirePackage{makeidx}
\RequirePackage{indentfirst}
\RequirePackage{geometry}
\RequirePackage{textcomp}

\geometry{a4paper, left=35mm, top=30mm, right=35mm, bottom=20mm}

\usepackage{helvet}
\renewcommand{\familydefault}{\sfdefault}
