\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{historia}[13/03/2017 Classe para uso em arquivos de caderno]
\LoadClass[a4paper, 12pt]{article}

\RequirePackage[utf8]{inputenc}
\RequirePackage[brazil]{babel}
\RequirePackage{makeidx}
\RequirePackage{indentfirst}
\RequirePackage{icomma}
\RequirePackage{multirow}
\RequirePackage{geometry}
\RequirePackage{textcomp}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{tikz}
\RequirePackage{pgfplots}
\RequirePackage{array}
\RequirePackage{enumitem}
\RequirePackage{multicol}
\RequirePackage{chemfig}
\RequirePackage{mhchem}
\RequirePackage{amssymb}
\RequirePackage{hyperref}
\RequirePackage{times}
\RequirePackage{microtype}
\RequirePackage{hyphenat}

\hypersetup{colorlinks, citecolor=black, filecolor=black, linkcolor=black, urlcolor=black}
\geometry{a4paper, left=30mm, top=15mm, right=15mm, bottom=15mm}
\setlist[itemize]{noitemsep}
\everymath{\displaystyle}

\renewcommand{\arraystretch}{1.3} % tamanho de tabelas

\setangleincrement{30}

\newcommand{\titulo}{
	\pagenumbering{gobble}
	\begin{titlepage}
	\begin{center}
		{\textbf{COLÉGIO CENECISTA FREDERICO MICHAELSEN}}\\\vspace{2cm}
		\@author\\\vspace{8cm}
		{\LARGE \@title}\\\vspace{5mm}
		{\Large ~~~~~~}\\
		\vfill
		Nova Petrópolis - RS\\
		2017
	\end{center}
	\end{titlepage}
	\pagenumbering{roman}
	\tableofcontents
	\clearpage
	\pagenumbering{arabic}
}

\let\stdpart\part
\renewcommand\part{\newpage\stdpart}


\newcommand{\referenciainternet}[2]{

\raggedleft \scriptsize Disponível em: #1,\\Acesso em: #2.\ignorespaces}


\newcommand{\testempty}[3]{% sintaxe:    1=texto a testar 2=cheio 3=vazio
\def\@tempa{#1}%
\ifx\@tempa\@empty#3\else#2\fi\ignorespaces}


\newcommand{\referencialivro}[7]{

\raggedleft \scriptsize {\scshape #1}, #2.
\textit{#3}.
\testempty{#4}{#4. ed. }{\relax}%
\testempty{#5}{#5: }{\relax}%
\testempty{#6}{#6, }{\relax}%
\testempty{#7}{#7.}{\relax}%
\ignorespaces}


\newcommand{\refer}[1]{~$^\text{[\ref{#1}~--~Pág. \pageref{#1}]}$}

\newcommand{\obra}[1]{``#1''}

\newcommand{\estr}[1]{\textit{#1}}


\renewenvironment{quote}{

\vspace{5mm plus 3mm minus 3mm}

\setlength{\leftskip}{4cm}

\noindent \small \ignorespaces}{\vspace{5mm plus 3mm minus 3mm}
    
\setlength{\leftskip}{0cm}}
